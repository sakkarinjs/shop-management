import sidebar from './sidebar/th'
import btn from './btn/th'
import barProfile from './barProfile/th'
import textfield from './textfield/th'
import alert from './alert/th'
import loading from './loading/th'

export default {
  sidebar,
  btn,
  barProfile,
  textfield,
  alert,
  loading,
  langId: 'th',
  langName: 'ไทย',
  languages: {
    thai: 'ไทย',
    english: 'English',
    china: '中文'
  }
}
