const sidebar = {
  textDashboard: '儀表板',
  textUserManagement: '用戶管理',
  textTransactionReport: '交易報告',
  textSystemLog: '系統日誌',
  textUserManagementAddAgent: '添加代理',
  textUserManagementDownlineList: '下線列表',
  textTransactionReportWinLose: '勝利失敗',
  textTransactionReportWinLoseDetails: '輸贏詳情',
  textTransactionReportWinLoseKiosk: '贏了 (Kiosk)',
  textTransactionReportOnlineUsers: '在線用戶',
  textTransactionReportPrizeTransactions: '獎勵交易',
  textSystemLogCreditlog: '信用日誌',
  textSystemLogViewLog: '查看日誌'
}

export default sidebar
