const sidebar = {
  textDashboard: 'หน้าหลัก',
  textUserManagement: 'การจัดการผู้ใช้',
  textTransactionReport: 'รายงานธุรกรรม',
  textSystemLog: 'บันทึกระบบ',
  textUserManagementAddAgent: 'เพิ่มตัวแทน',
  textUserManagementDownlineList: 'รายการดาวน์ไลน์',
  textTransactionReportWinLose: 'แพ้ชนะ',
  textTransactionReportWinLoseDetails: 'ชนะรายละเอียดที่สูญเสีย',
  textTransactionReportWinLoseKiosk: 'ชนะ แพ้ (Kiosk)',
  textTransactionReportOnlineUsers: 'ผู้ใช้งานออนไลน์',
  textTransactionReportPrizeTransactions: 'รางวัลการทำธุรกรรม',
  textSystemLogCreditlog: 'บันทึกเครดิต',
  textSystemLogViewLog: 'ดูบันทึก'
}

export default sidebar
