const configuregeneral = {
  urlService: '',
  urlImage: '',
  keySessionStorage: '_shop_management',
  siteName: 'shop management',
  firebaseCollection: 'user',
  firebaseDoc: 'user'
}

export default configuregeneral
