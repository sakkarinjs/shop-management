export const state = () => ({
  getUser: null
})

export const getters = {
  getUser (state) {
    return state.getUser
  }
}

export const mutations = {
  getUser (state, payload) {
    state.getUser = payload
  }
}

export const actions = {
  onAuthStateChangedAction (state, { authUser, claims }) {
    if (!authUser) {
      state.commit('getUser', null)
      this.$router.push({
        path: '/login'
      })
    } else {
      state.commit('getUser', authUser.refreshToken)
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
