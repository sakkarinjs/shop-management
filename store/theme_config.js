import theme from './theme/index'

export const state = () => ({
  setTheme: '',
  setThemeVuetify: ''
})

export const getters = {
  setTheme: state => state.setTheme,
  setThemeVuetify: state => state.setThemeVuetify
}

export const mutations = {
  setTheme (
    state,
    payload
  ) {
    state.setTheme = payload
  },
  setThemeVuetify (
    state,
    payload
  ) {
    state.setThemeVuetify = payload
  }
}

export const actions = {
  setStoreDataTheme ({ commit, dispatch }, item) {
    commit('setTheme', theme.defaultcolor)
  },
  setStoreDataThemeVuetify ({ commit, dispatch }, item) {
    commit('setThemeVuetify', item)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
