import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '../assets/fontawesome/css/all.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'fa' || 'faSvg' || 'mdi'
  }
})
